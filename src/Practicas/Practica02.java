package Practicas;


import Practicas.Cotizacion;


public class Practica02 {

    
    public static void main(String[] args) {
        //Generar objeto construido por omisicion
        Cotizacion Practica02 = new Cotizacion();
        Practica02.setFolio("147");
        Practica02.setDescripcion("toyota negro");
        Practica02.setPrecio(220000.0f);
        Practica02.setPorcentajeInicial(0.25f);
        Practica02.setPlazo(36);
        
        System.out.println("Pago inicial= "+Practica02.calcularPagoIn());
        System.out.println("Total= "+Practica02.calcularPagoMen());
        System.out.println("Pago mensual= "+Practica02.calcularTotalFin());
    }
    
}
